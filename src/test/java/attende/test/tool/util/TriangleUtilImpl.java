package attende.test.tool.util;


import org.springframework.stereotype.Component;

@Component
public class TriangleUtilImpl implements TriangleUtil {
    @Override
    public double countArea(double a, double b, double c) {
        double halfPerimeter = countPerimeter(a, b, c)/2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c));
    }

    @Override
    public double countPerimeter(double a, double b, double c) {
        return a + b + c;
    }
}
