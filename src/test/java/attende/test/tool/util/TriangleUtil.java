package attende.test.tool.util;

public interface TriangleUtil {

    double countArea(double a, double b, double c);
    double countPerimeter(double a, double b, double c);

}
