package attende.test.tool.integration;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import static org.springframework.http.MediaType.APPLICATION_JSON;


@Component
public class TriangleServiceIntegrationImpl implements TriangleServiceIntegration {

    private static final Logger LOG = LoggerFactory.getLogger(TriangleServiceIntegrationImpl.class);


    private static final String USER_TOKEN = "f2846c03-cd5c-4f06-b43d-d59b5afa71cd";
    private static final String TRIANGLE_SERVICE_URL = "http://akrigator.synology.me:8888";


    private static final String TRIANGLES_PATH = "/triangle/all";
    private static final String TRIANGLE_PATH = "/triangle";
    private static final String PERIMETER_PATH = "/perimeter";
    private static final String AREA_PATH = "/area";

    @Autowired
    @Qualifier("triangleServiceRestTemplate")
    private RestTemplate restTemplate;


    @Override
    public String createNewTriangle(String payload) {
        HttpEntity<String> entity = new HttpEntity<>(payload, getHeaders());
        String response = restTemplate.postForObject(TRIANGLE_SERVICE_URL + TRIANGLE_PATH, entity, String.class);
        LOG.debug("Triangle created {}", response);
        return response;
    }

    @Override
    public String getTriangleById(String id) {
        HttpEntity<String> entity = new HttpEntity<>(getHeaders());
        ResponseEntity<String> response = restTemplate.exchange(
                TRIANGLE_SERVICE_URL + TRIANGLE_PATH + id, HttpMethod.GET, entity, String.class);
        LOG.debug("Triangle with id {} recieved {}", id, response.getBody());
        return response.getBody();
    }

    @Override
    public void deleteTriangleById(String id) {
        HttpEntity<String> entity = new HttpEntity<>(getHeaders());
        ResponseEntity<String> response = restTemplate.exchange(
                TRIANGLE_SERVICE_URL + TRIANGLE_PATH + "/" + id, HttpMethod.DELETE, entity, String.class);
        LOG.debug("Triangle with id {} deleted", id);
//        response.getBody();
    }

    @Override
    public String getAllTriangles() {
        HttpEntity<String> entity = new HttpEntity<>(getHeaders());
        ResponseEntity<String> response = restTemplate.exchange(
                TRIANGLE_SERVICE_URL + TRIANGLES_PATH, HttpMethod.GET, entity, String.class);
        LOG.debug("Triangles recieved {}", response.getBody());
        return response.getBody();
    }

    @Override
    public String getTrianglePerimeterByTriangleId(String id) {
        HttpEntity<String> entity = new HttpEntity<>(getHeaders());
        ResponseEntity<String> response = restTemplate.exchange(
                TRIANGLE_SERVICE_URL + TRIANGLE_PATH + "/" + id + "/" + PERIMETER_PATH, HttpMethod.GET, entity, String.class);
        LOG.debug("Triangle {} perimeter recieved {}", id, response.getBody());
        return response.getBody();
    }

    @Override
    public String getTriangleSquareById(String id) {

        HttpEntity<String> entity = new HttpEntity<>(getHeaders());
        ResponseEntity<String> response = restTemplate.exchange(
                TRIANGLE_SERVICE_URL + TRIANGLE_PATH + "/" + id + "/" + AREA_PATH, HttpMethod.GET, entity, String.class);
        LOG.debug("Triangle {} area recieved {}", id, response.getBody());
        return response.getBody();
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        headers.set("X-User", USER_TOKEN);
        return headers;
    }
}
