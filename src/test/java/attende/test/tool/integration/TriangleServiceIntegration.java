package attende.test.tool.integration;

public interface TriangleServiceIntegration {

    String createNewTriangle(String payload);
    String getTriangleById(String id);
    void deleteTriangleById(String id);
    String getAllTriangles();
    String getTrianglePerimeterByTriangleId(String id);
    String getTriangleSquareById(String id);

}
