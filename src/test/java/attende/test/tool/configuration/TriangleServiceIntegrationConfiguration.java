package attende.test.tool.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TriangleServiceIntegrationConfiguration {

    @Bean(name = {"triangleServiceRestTemplate"})
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
