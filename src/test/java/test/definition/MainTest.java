package test.definition;


import attende.test.tool.SpringBootApplication;
import attende.test.tool.integration.TriangleServiceIntegrationImpl;
import com.jayway.jsonpath.JsonPath;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import attende.test.tool.util.TriangleUtilImpl;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApplication.class)
public class MainTest {


    @Autowired
    private TriangleServiceIntegrationImpl triangleServiceIntegration;

    @Autowired
    private TriangleUtilImpl triangleUtil;


    @Before
    public void cleaunUp() {
        List<String> triangleList = JsonPath.read(triangleServiceIntegration.getAllTriangles(), "$..id");
        if (!triangleList.isEmpty()) {
            triangleList.forEach(triangle -> triangleServiceIntegration.deleteTriangleById(triangle));
        }
    }

    @Test
    public void validatePerimeter() {
        double a = 3;
        double b = 4;
        double c = 5;
        String payload = String.format("{\"separator\": \";\", \"input\": \"%d;%d;%d\"}", (int) a, (int) b, (int) c);
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        double perimeterSize = JsonPath.read(triangleServiceIntegration.getTrianglePerimeterByTriangleId(triangleId), "$.result");
        double expectedPerimeter = triangleUtil.countPerimeter(a, b, c);
        Assert.assertEquals(expectedPerimeter, perimeterSize, 0.0);
    }

    @Test
    public void validatePerimeterWithFloatSides() {
        double a = 3.4;
        double b = 4.5;
        double c = 5.5;
        String payload = String.format("{\"separator\": \";\", \"input\": \"%s;%s;%s\"}", String.valueOf(a), String.valueOf(b), String.valueOf(c));
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        double perimeterSize = JsonPath.read(triangleServiceIntegration.getTrianglePerimeterByTriangleId(triangleId), "$.result");
        double expectedPerimeter = triangleUtil.countPerimeter(a, b, c);
        Assert.assertEquals(expectedPerimeter, perimeterSize, 0.0);
    }



    @Test
    public void validateArea() {
        double a = 3;
        double b = 4;
        double c = 5;
        String payload = String.format("{\"separator\": \";\", \"input\": \"%d;%d;%d\"}", (int) a, (int) b, (int) c);
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        double areaSize = JsonPath.read(triangleServiceIntegration.getTriangleSquareById(triangleId), "$.result");
        double expectedAreaSize = triangleUtil.countArea(a, b, c);
        Assert.assertEquals(expectedAreaSize, areaSize, 0.0);
    }

    @Test
    public void validateAreaWithFloatSides() {
        double a = 3.4;
        double b = 4.5;
        double c = 5.5;
        String payload = String.format("{\"separator\": \";\", \"input\": \"%s;%s;%s\"}", String.valueOf(a), String.valueOf(b), String.valueOf(c));
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        double areaSize = JsonPath.read(triangleServiceIntegration.getTriangleSquareById(triangleId), "$.result");
        double expectedAreaSize = triangleUtil.countArea(a, b, c);
        Assert.assertEquals(expectedAreaSize, areaSize, 0.0);
    }


    @Test(expected = HttpClientErrorException.class)
    public void assertThatTriangleServerLimitsAmountOfTriangles() {
        String payload = "{\"separator\": \";\", \"input\": \"3;4;5\"}";
        for (int i = 0; i < 12; i++) {
            triangleServiceIntegration.createNewTriangle(payload);
        }
    }


    @Test(expected = HttpClientErrorException.class)
    public void assertThatTriangleBeingDeleted() {
        String payload = "{\"separator\": \";\", \"input\": \"3;4;5\"}";
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        triangleServiceIntegration.deleteTriangleById(triangleId);
        List<String> triangleList = JsonPath.read(triangleServiceIntegration.getAllTriangles(), "$..id");
        Assert.assertTrue(triangleList.stream().noneMatch(triangle -> triangle.equals(triangleId)));
        triangleServiceIntegration.getTriangleById(triangleId);
    }

    @Test(expected = HttpClientErrorException.class)
    public void assertThatTriangleBeingAdded() {
        String payload = "{\"separator\": \";\", \"input\": \"3;4;5\"}";
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        List<String> triangleList = JsonPath.read(triangleServiceIntegration.getAllTriangles(), "$..id");
        Assert.assertTrue(triangleList.stream().anyMatch(triangle -> triangle.equals(triangleId)));
        triangleServiceIntegration.getTriangleById(triangleId);
    }


    @Test(expected = HttpClientErrorException.class)
    public void assertThatTriangleServiceDeniesPayloadWithNegativeSides() {
        String payload = "{\"separator\": \";\", \"input\": \"-13;4;5\"}";
        triangleServiceIntegration.createNewTriangle(payload);
    }

    @Test(expected = HttpClientErrorException.class)
    public void assertThatTriangleServiceDeniesPayloadWithInvalidSides() {
        String payload = "{\"separator\": \";\", \"input\": \"1;1;10\"}";
        triangleServiceIntegration.createNewTriangle(payload);
    }

    @Test(expected = HttpClientErrorException.class)
    public void assertThatTriangleServiceDeniesPayloadWithRandomSeparator() {
        String payload = "{\"input\": \"3.4.5\"}";
        triangleServiceIntegration.createNewTriangle(payload);
    }

    @Test
    @Ignore
    public void assertThatTriangleServiceDeniesPayloadWithDegenerateTriangle() {
        String payload = "{\"separator\": \";\", \"input\": \"5;5;10\"}";
        triangleServiceIntegration.createNewTriangle(payload);
    }

    @Test
    public void validateAreaofDegenerateTriangle() {
        double a = 5;
        double b = 5;
        double c = 10;
        String payload = String.format("{\"separator\": \";\", \"input\": \"%s;%s;%s\"}", (int) a, (int) b, (int) c);
        String triangleId = JsonPath.read(triangleServiceIntegration.createNewTriangle(payload), "$.id");
        double arearSize = JsonPath.read(triangleServiceIntegration.getTriangleSquareById(triangleId), "$.result");
        double expectedAreaSize = 0;
        Assert.assertEquals(expectedAreaSize, arearSize, 0.0);
    }


}
